const I_TERM_WIDGET = {
    NAME: '{Name}',
    WEATHER_API_KEY: '{OpenWeatherMap API key}',
    WORK_CALENDAR_NAME: '{Work Calendar Name}',
    PERSONAL_CALENDAR_NAME: '{Personal Calendar Name}',
    PERIOD_CALENDAR_NAME: '{Period Calendar Name}',
    PERIOD_EVENT_NAME: '{Period Event Name',
    DOG_FOOD_URL: '{Dog Food Url}'
}

module.exports.I_TERM_WIDGET = I_TERM_WIDGET;