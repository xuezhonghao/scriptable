// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: blue; icon-glyph: paw;
// Checks if Slack is down by examining their status page. Works perfectly with Siri.

class DogFood {
  constructor() {
    const userData = importModule('userData');
    const I_TERM_WIDGET = userData.I_TERM_WIDGET;
    this.DOG_FOOD_URL = I_TERM_WIDGET.DOG_FOOD_URL;
  }

  async main() {
    let url = this.DOG_FOOD_URL;
    let r = new Request(url);
    let bodyString = await r.loadString();
    let body = JSON.parse(bodyString);
    let message = JSON.parse(body.message);
    console.log(message);
    Speech.speak(message.amount_real + " grams");
    Script.complete();
  }
}

module.exports = DogFood;